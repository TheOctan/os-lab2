﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task1
{
    public partial class Form1 : Form
    {
        private Thread thread1;
        private Thread thread2;

        private int turn = 0;
        private string message = "";

        public Form1()
        {
            InitializeComponent();

            thread1 = new Thread(Task1);
            thread2 = new Thread(Task2);            
        }

        public void Stop()
        {
            thread1.Abort();
            thread2.Abort();
        }

        void function()
        {

        }

        private void Task1()
        {
            while (true)
            {
                while (turn != 0) ;

                // critical region
                Action action = () => label1.Text = "Task1 is ready";
                Invoke(action);
                Thread.Sleep(1000);


                bool success = int.TryParse(message, out int number);

                turn = 1;

                // noncritical region
                action = delegate ()
                {
                    if (number >= 1 && number <= 3)
                    {
                        for (int i = 0; i < number; i++)
                        {
                            SystemSounds.Beep.Play();
                            Thread.Sleep(500);
                        }
                    }
                };
                Invoke(action);
            }
        }

        private void Task2()
        {
            while (true)
            {
                while (turn != 1) ;

                // critical region
                Action action = () => label1.Text = "Task2 is ready";
                Invoke(action);
                Thread.Sleep(1000);

                char symbol = 'x';
                if (message.Length != 0)
                    symbol = message.First();

                turn = 0;

                // noncritical region

                action = () => textBox2.Text = ((char)(symbol + 2)).ToString();
                Invoke(action);
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                message = textBox1.Text;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            thread1.Start();
            thread2.Start();
        }
    }
}
